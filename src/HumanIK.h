#ifndef HUMANIK_H
#define HUMANIK_H

#include <Godot.hpp>
#include <Node.hpp>
#include <Skeleton.hpp>
#include <vector>

namespace godot
{

class HumanIK : public Node
{
    GODOT_CLASS(HumanIK, Node)

private:
    float time;

    NodePath skeletonPath;
    Skeleton *skeleton;

    Transform headTarget;
    Transform handLeftTarget;
    Transform handRightTarget;
    Transform hipTarget;
    Transform footLeftTarget;
    Transform footRightTarget;

    NodePath headTargetNode;
    NodePath handLeftTargetNode;
    NodePath handRightTargetNode;
    NodePath hipTargetNode;
    NodePath footLeftTargetNode;
    NodePath footRightTargetNode;

    Spatial *headTargetSpatial;
    Spatial *handLeftTargetSpatial;
    Spatial *handRightTargetSpatial;
    Spatial *hipTargetSpatial;
    Spatial *footLeftTargetSpatial;
    Spatial *footRightTargetSpatial;

    float floorHeight;

    int64_t headBone;
    PoolIntArray neckBones; //array of neck bones
    int64_t shoulderLeftBone;
    int64_t upperArmLeftBone;
    int64_t forearmLeftBone;
    int64_t handLeftBone;
    int64_t shoulderRightBone;
    int64_t upperArmRightBone;
    int64_t forearmRightBone;
    int64_t handRightBone;
    PoolIntArray spineBones; //array of spine bones
    int64_t hipBone;
    int64_t thighLeftBone;
    int64_t shinLeftBone;
    int64_t footLeftBone;
    int64_t thighRightBone;
    int64_t shinRightBone;
    int64_t footRightBone;

    float crouchBendFactor;
    float shoulderFlex;
    float armStretch;
    float legStretch;

    float elbowAngleOffset;
    float elbowRestOffset;
    float shoulderTwist;
    float wristTwist;
    Vector3 elbowPoleOffset;
    Vector3 elbowPositionBias;
    float elbowRotationBias;

    float kneeAngleOffset;
    float kneeRestOffset;
    float hipTwist;
    float ankleTwist;
    Vector3 kneePoleOffset;
    Vector3 kneePositionBias;
    float kneeRotationBias;

    float shoulderTurnSensitivity;
    float shoulderTurnLimit;
    float hipTurnSensitivity;
    float hipTurnLimit;
    float turnResetSpeed;

    bool validateBone(int64_t bone);
    bool validateBody();
    bool validateLeftHand();
    bool validateRightHand();
    bool validateLeftFoot();
    bool validateRightFoot();

    bool validBody;
    bool validLeftHand;
    bool validRightHand;
    bool validLeftFoot;
    bool validRightFoot;

    bool headTrackerEnabled;
    bool leftHandTrackerEnabled;
    bool rightHandTrackerEnabled;
    bool hipTrackerEnabled;
    bool leftFootTrackerEnabled;
    bool rightFootTrackerEnabled;

    String boneAssignmentsGroup;
    String IKAdjustmentsGroup;

    //SIFABRIK
    std::vector<Transform> spineCurve;
    float curveDist;
    float spineLength;
    Vector2 curveAxis;
    struct BonePoint
    {
        BonePoint(float l, Transform t, Vector3 p)
        {
            length = l;
            transform = t;
            point = p;
        }
        BonePoint()
        {
            length = 0;
            transform = Transform();
            point = Vector3();
        }
        float length;
        Transform transform; //local
        Vector3 point;       //global
    };

    //Leg Tracing Variables
    Vector3 centerFloor;  //where the center of gravity is over the floor
    Vector3 centerVector; //direction and speed centerFloor is moving
    Vector3 leftStep;     //The ideal new foot position
    Vector3 rightStep;
    float leftProgress; //How far along the leg is in taking the step. Measured in time. Goes from 0 to leftStepTime.
    float rightProgress;
    float leftStepTime; //Total time available to take the step. As centerVector scales up, this value increases
    float rightStepTime;
    float maxStepTime;
    float minStepTime;
    float footLength;
    float maxAnkleBend;
    float relaxAmount; //when the foot is dangling how much do we bend everything

    static float smoothCurve(float number);
    static Vector3 normalProjection(Vector3 v, Vector3 normal);
    static void HumanIK::crossDot(Vector3 &cross, float &dot, Vector3 srcVector1, Vector3 srcVector2);

    //for SIFABRIK
    void calcSpineCurve();
    static void solveFABRIKPoints(std::vector<HumanIK::BonePoint> &bonePoints, Vector3 rootPoint, Vector3 goal, float threshold, int loopLimit);
    static Vector3 fitPointToLine(Vector3 point, Vector3 goal, float length);

public:
    static void _register_methods();
    void _ready();
    //void _validate_property(Skeleton::PropertyInfo &property);

    HumanIK();
    ~HumanIK();

    void _init(); // our initializer called by Godot

    void _physics_process(float delta);

    void performBodyIK(float influence = 1);
    void performLeftHandIK(float influence = 1);
    void performRightHandIK(float influence = 1);
    void performLeftFootIK(float influence = 1);
    void performRightFootIK(float influence = 1);

    NodePath getSkeletonPath();
    void setSkeletonPath(NodePath newValue);

    NodePath getHeadTargetNode();
    NodePath getHandLeftTargetNode();
    NodePath getHandRightTargetNode();
    NodePath getHipTargetNode();
    NodePath getFootLeftTargetNode();
    NodePath getFootRightTargetNode();

    void setHeadTargetNode(NodePath newValue);
    void setHandLeftTargetNode(NodePath newValue);
    void setHandRightTargetNode(NodePath newValue);
    void setHipTargetNode(NodePath newValue);
    void setFootLeftTargetNode(NodePath newValue);
    void setFootRightTargetNode(NodePath newValue);

    int64_t getHeadBone();
    PoolIntArray getNeckBones();
    int64_t getShoulderLeftBone();
    int64_t getUpperArmLeftBone();
    int64_t getForearmLeftBone();
    int64_t getHandLeftBone();
    int64_t getShoulderRightBone();
    int64_t getUpperArmRightBone();
    int64_t getForearmRightBone();
    int64_t getHandRightBone();
    PoolIntArray getSpineBones();
    int64_t getHipBone();
    int64_t getThighLeftBone();
    int64_t getShinLeftBone();
    int64_t getFootLeftBone();
    int64_t getThighRightBone();
    int64_t getShinRightBone();
    int64_t getFootRightBone();

    void setHeadBone(int64_t newValue);
    void setNeckBones(PoolIntArray newValue);
    void setShoulderLeftBone(int64_t newValue);
    void setUpperArmLeftBone(int64_t newValue);
    void setForearmLeftBone(int64_t newValue);
    void setHandLeftBone(int64_t newValue);
    void setShoulderRightBone(int64_t newValue);
    void setUpperArmRightBone(int64_t newValue);
    void setForearmRightBone(int64_t newValue);
    void setHandRightBone(int64_t newValue);
    void setSpineBones(PoolIntArray newValue);
    void setHipBone(int64_t newValue);
    void setThighLeftBone(int64_t newValue);
    void setShinLeftBone(int64_t newValue);
    void setFootLeftBone(int64_t newValue);
    void setThighRightBone(int64_t newValue);
    void setShinRightBone(int64_t newValue);
    void setFootRightBone(int64_t newValue);

    struct TrigSolution
    {
        Transform limb1;
        Transform limb2;
    };

    static TrigSolution solveTrigIK(Transform parent, Transform limb1, Transform limb2, Transform leaf, Transform target, float angleOffset, float restOffset, float l1Twist, float l2Twist, Vector3 poleOffset, Vector3 targetPositionInfluence, float targetRotationInfluence);

    static std::vector<Transform> solveISFABRIK(Transform parent, std::vector<Transform> restTransforms, Transform leaf, Transform target, std::vector<Transform> curveTransforms, float curveDist, float maxDist, float threshold, int loopLimit, float twist, float twistOffset);

    static std::vector<Transform> solveFABRIK(Transform parent, std::vector<Transform> restTransforms, Transform leaf, Transform target, float threshold, int loopLimit, float twist, float twistOffset);

    void LegTrace();
    //All used in leg trace
    bool checkBalance();
    bool checkFall();
};

} // namespace godot

#endif
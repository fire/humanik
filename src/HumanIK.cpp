#include "HumanIK.h"
#include <vector>
//#include "HumanIKSolver.h"
//#include "HumanIKSolver.cpp" //get rid of linking issues

using namespace godot;

void HumanIK::_register_methods()
{
    register_property<HumanIK, bool>("", &HumanIK::validBody, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, bool>("", &HumanIK::validLeftHand, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, bool>("", &HumanIK::validRightHand, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, bool>("", &HumanIK::validLeftFoot, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, bool>("", &HumanIK::validRightFoot, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);

    register_property<HumanIK, bool>("", &HumanIK::headTrackerEnabled, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, bool>("", &HumanIK::leftHandTrackerEnabled, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, bool>("", &HumanIK::rightHandTrackerEnabled, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, bool>("", &HumanIK::hipTrackerEnabled, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, bool>("", &HumanIK::leftFootTrackerEnabled, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, bool>("", &HumanIK::rightFootTrackerEnabled, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);

    register_property<HumanIK, Skeleton *>("", &HumanIK::skeleton, nullptr, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);

    register_property<HumanIK, Spatial *>("", &HumanIK::headTargetSpatial, nullptr, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Spatial *>("", &HumanIK::handLeftTargetSpatial, nullptr, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Spatial *>("", &HumanIK::handRightTargetSpatial, nullptr, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Spatial *>("", &HumanIK::hipTargetSpatial, nullptr, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Spatial *>("", &HumanIK::footLeftTargetSpatial, nullptr, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Spatial *>("", &HumanIK::footRightTargetSpatial, nullptr, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);

    register_property<HumanIK, NodePath>("Skeleton", &HumanIK::setSkeletonPath, &HumanIK::getSkeletonPath, NodePath(".."));

    register_property<HumanIK, Transform>("Head Target", &HumanIK::headTarget, Transform(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Transform>("Left Hand Target", &HumanIK::handLeftTarget, Transform(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Transform>("Right Hand Target", &HumanIK::handRightTarget, Transform(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Transform>("Hip Target", &HumanIK::hipTarget, Transform(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Transform>("Left Foot Target", &HumanIK::footLeftTarget, Transform(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Transform>("Right Foot Target", &HumanIK::footRightTarget, Transform(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);

    register_property<HumanIK, NodePath>("Head Target", &HumanIK::setHeadTargetNode, &HumanIK::getHeadTargetNode, NodePath());
    register_property<HumanIK, NodePath>("Left Hand Target", &HumanIK::setHandLeftTargetNode, &HumanIK::getHandLeftTargetNode, NodePath());
    register_property<HumanIK, NodePath>("Right Hand Target", &HumanIK::setHandRightTargetNode, &HumanIK::getHandRightTargetNode, NodePath());
    register_property<HumanIK, NodePath>("Hip Target", &HumanIK::setHipTargetNode, &HumanIK::getHipTargetNode, NodePath());
    register_property<HumanIK, NodePath>("Left Foot Target", &HumanIK::setFootLeftTargetNode, &HumanIK::getFootLeftTargetNode, NodePath());
    register_property<HumanIK, NodePath>("Right Foot Target", &HumanIK::setFootRightTargetNode, &HumanIK::getFootRightTargetNode, NodePath());

    register_property<HumanIK, String>("Bone Assignments", &HumanIK::boneAssignmentsGroup, "", GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_GROUP);
    register_property<HumanIK, int64_t>("Head Bone Index", &HumanIK::setHeadBone, &HumanIK::getHeadBone, -1);
    register_property<HumanIK, PoolIntArray>("Neck Bone Indexes", &HumanIK::setNeckBones, &HumanIK::getNeckBones, PoolIntArray());
    register_property<HumanIK, int64_t>("Shoulder Left Bone Index", &HumanIK::setShoulderLeftBone, &HumanIK::getShoulderLeftBone, -1);
    register_property<HumanIK, int64_t>("Upper Arm Left Bone Index", &HumanIK::setUpperArmLeftBone, &HumanIK::getUpperArmLeftBone, -1);
    register_property<HumanIK, int64_t>("Forearm Left Bone Index", &HumanIK::setForearmLeftBone, &HumanIK::getForearmLeftBone, -1);
    register_property<HumanIK, int64_t>("Hand Left Bone Index", &HumanIK::setHandLeftBone, &HumanIK::getHandLeftBone, -1);
    register_property<HumanIK, int64_t>("Shoulder Right Bone Index", &HumanIK::setShoulderRightBone, &HumanIK::getShoulderRightBone, -1);
    register_property<HumanIK, int64_t>("Upper Arm Right Bone Index", &HumanIK::setUpperArmRightBone, &HumanIK::getUpperArmRightBone, -1);
    register_property<HumanIK, int64_t>("Forearm Right Bone Index", &HumanIK::setForearmRightBone, &HumanIK::getForearmRightBone, -1);
    register_property<HumanIK, int64_t>("Hand Right Bone Index", &HumanIK::setHandRightBone, &HumanIK::getHandRightBone, -1);
    register_property<HumanIK, PoolIntArray>("Spine Bone Indexes", &HumanIK::setSpineBones, &HumanIK::getSpineBones, PoolIntArray());
    register_property<HumanIK, int64_t>("Hip Bone Index", &HumanIK::setHipBone, &HumanIK::getHipBone, -1);
    register_property<HumanIK, int64_t>("Thigh Left Bone Index", &HumanIK::setThighLeftBone, &HumanIK::getThighLeftBone, -1);
    register_property<HumanIK, int64_t>("Shin Left Bone Index", &HumanIK::setShinLeftBone, &HumanIK::getShinLeftBone, -1);
    register_property<HumanIK, int64_t>("Foot Left Bone Index", &HumanIK::setFootLeftBone, &HumanIK::getFootLeftBone, -1);
    register_property<HumanIK, int64_t>("Thigh Right Bone Index", &HumanIK::setThighRightBone, &HumanIK::getThighRightBone, -1);
    register_property<HumanIK, int64_t>("Shin Right Bone Index", &HumanIK::setShinRightBone, &HumanIK::getShinRightBone, -1);
    register_property<HumanIK, int64_t>("Foot Right Bone Index", &HumanIK::setFootRightBone, &HumanIK::getFootRightBone, -1);

    register_property<HumanIK, String>("IK Adjustments", &HumanIK::IKAdjustmentsGroup, "", GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_GROUP);
    register_property<HumanIK, float>("Floor Height", &HumanIK::floorHeight, 0.0);
    register_property<HumanIK, float>("Crouch Bend Factor", &HumanIK::crouchBendFactor, 50.0);
    register_property<HumanIK, float>("Shoulder Flex Multiplier", &HumanIK::shoulderFlex, 10.0);
    register_property<HumanIK, float>("Arm Stretch Multiplier", &HumanIK::armStretch, 0.0);
    register_property<HumanIK, float>("Leg Stretch Multiplier", &HumanIK::legStretch, 0.0);

    register_property<HumanIK, float>("Elbow Angle Offset", &HumanIK::elbowAngleOffset, 45.0);
    register_property<HumanIK, float>("Elbow Rest Offset", &HumanIK::elbowRestOffset, -45.0);
    register_property<HumanIK, float>("Shoulder Twisting", &HumanIK::shoulderTwist, 50);
    register_property<HumanIK, float>("Wrist Twisting", &HumanIK::wristTwist, 66.666);
    register_property<HumanIK, Vector3>("Elbow Pole Offset", &HumanIK::elbowPoleOffset, Vector3(0, -90, 0));
    register_property<HumanIK, Vector3>("Hand Position Elbow Influence", &HumanIK::elbowPositionBias, Vector3(100, 0, -50.0));
    register_property<HumanIK, float>("Hand Rotation Elbow Influence", &HumanIK::elbowRotationBias, 75.0);
    register_property<HumanIK, float>("Knee Angle Offset", &HumanIK::kneeAngleOffset, 180.0);
    register_property<HumanIK, float>("Knee Rest Offset", &HumanIK::kneeRestOffset, 0.0);
    register_property<HumanIK, float>("Hip Twisting", &HumanIK::hipTwist, 100);
    register_property<HumanIK, float>("Ankle Twisting", &HumanIK::ankleTwist, 80);
    register_property<HumanIK, Vector3>("Knee Pole Offset", &HumanIK::kneePoleOffset, Vector3(-30, -20, 0));
    register_property<HumanIK, Vector3>("Foot Position Knee Influence", &HumanIK::kneePositionBias, Vector3(0, 0, 30));
    register_property<HumanIK, float>("Foot Rotation Knee Influence", &HumanIK::kneeRotationBias, 50.0);

    register_property<HumanIK, Vector2>("Spine Curve Axis", &HumanIK::curveAxis, Vector2(-1, 0));
    register_property<HumanIK, float>("Shoulder Turn Sensitivity", &HumanIK::shoulderTurnSensitivity, 50.0);
    register_property<HumanIK, float>("Shoulder Turn Limit", &HumanIK::shoulderTurnLimit, 0.0);
    register_property<HumanIK, float>("Hip Turn Sensitivity", &HumanIK::hipTurnSensitivity, 50.0);
    register_property<HumanIK, float>("Hip Turn Limit", &HumanIK::hipTurnLimit, 0.0);
    register_property<HumanIK, float>("Turn Reset Speed", &HumanIK::turnResetSpeed, 1.0);

    register_property<HumanIK, bool>("Enable Head Tracker", &HumanIK::headTrackerEnabled, false);
    register_property<HumanIK, bool>("Enable Left Hand Tracker", &HumanIK::leftHandTrackerEnabled, false);
    register_property<HumanIK, bool>("Enable Right Hand Tracker", &HumanIK::rightHandTrackerEnabled, false);
    register_property<HumanIK, bool>("Enable Hip Tracker", &HumanIK::hipTrackerEnabled, false);
    register_property<HumanIK, bool>("Enable Left Foot Tracker", &HumanIK::leftFootTrackerEnabled, false);
    register_property<HumanIK, bool>("Enable Right Foot Tracker", &HumanIK::rightFootTrackerEnabled, false);

    register_method("_physics_process", &HumanIK::_physics_process);
    register_method("_ready", &HumanIK::_ready);

    register_method("performBodyIK", &HumanIK::performBodyIK);
    register_method("performLeftHandIK", &HumanIK::performLeftHandIK);
    register_method("performRightHandIK", &HumanIK::performRightHandIK);
    register_method("performLeftFootIK", &HumanIK::performLeftFootIK);
    register_method("performRightFootIK", &HumanIK::performRightFootIK);
}

HumanIK::HumanIK()
{
}

HumanIK::~HumanIK()
{
    // add your cleanup here
}

void HumanIK::_init()
{
    // initialize any variables here
    time = 0.0;
    validBody = false;
    validLeftHand = false;
    validRightHand = false;
    validLeftFoot = false;
    validRightFoot = false;

    headTrackerEnabled = false;
    leftHandTrackerEnabled = false;
    rightHandTrackerEnabled = false;
    hipTrackerEnabled = false;
    leftFootTrackerEnabled = false;
    rightFootTrackerEnabled = false;

    skeleton = nullptr;

    headTargetSpatial = nullptr;
    handLeftTargetSpatial = nullptr;
    handRightTargetSpatial = nullptr;
    hipTargetSpatial = nullptr;
    footLeftTargetSpatial = nullptr;
    footRightTargetSpatial = nullptr;

    Node *parent = this->get_parent();
    skeletonPath = NodePath("..");
    if (parent != nullptr)
    {
        skeletonPath = parent->get_path();
    }
    skeleton = nullptr;

    Transform zeroTransform = Transform(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    headTarget = zeroTransform;
    handLeftTarget = zeroTransform;
    handRightTarget = zeroTransform;
    hipTarget = zeroTransform;
    footLeftTarget = zeroTransform;
    footRightTarget = zeroTransform;

    NodePath zeroPath = NodePath("");
    headTargetNode = zeroPath;
    handLeftTargetNode = zeroPath;
    handRightTargetNode = zeroPath;
    hipTargetNode = zeroPath;
    footLeftTargetNode = zeroPath;
    footRightTargetNode = zeroPath;

    headTargetSpatial = nullptr;
    handLeftTargetSpatial = nullptr;
    handRightTargetSpatial = nullptr;
    hipTargetSpatial = nullptr;
    footLeftTargetSpatial = nullptr;
    footRightTargetSpatial = nullptr;

    floorHeight = 0.0;

    headBone = -1;
    neckBones = PoolIntArray(); //array of neck bones
    shoulderLeftBone = -1;
    upperArmLeftBone = -1;
    forearmLeftBone = -1;
    handLeftBone = -1;
    shoulderRightBone = -1;
    upperArmRightBone = -1;
    forearmRightBone = -1;
    handRightBone = -1;
    spineBones = PoolIntArray(); //array of spine bones
    hipBone = -1;
    thighLeftBone = -1;
    shinLeftBone = -1;
    footLeftBone = -1;
    thighRightBone = -1;
    shinRightBone = -1;
    footRightBone = -1;

    crouchBendFactor = 50.0;
    shoulderFlex = 10.0;
    armStretch = 0.0;
    legStretch = 0.0;

    elbowAngleOffset = 45.0;
    elbowRestOffset = -45.0;
    shoulderTwist = 50;
    wristTwist = 66.666;
    elbowPoleOffset = Vector3(0, -90, 0);
    elbowPositionBias = Vector3(100, 0, -50.0); //debug values
    elbowRotationBias = 75.0;
    kneeAngleOffset = 180.0;
    kneeRestOffset = 0.0;
    hipTwist = 100;
    ankleTwist = 80;
    kneePoleOffset = Vector3(-30, -20, 0);
    kneePositionBias = Vector3(0, 0, 30);
    kneeRotationBias = 50.0;

    shoulderTurnSensitivity = 50.0;
    shoulderTurnLimit = 0.0;
    hipTurnSensitivity = 50.0;
    hipTurnLimit = 0.0;
    turnResetSpeed = 1.0;

    String boneAssignmentsGroup = "";
    String IKAdjustmentsGroup = "";

    spineCurve = std::vector<Transform>(7);
    curveDist = 0;
    spineLength = 0;
    curveAxis = Vector2(-1, 0);
}

void HumanIK::_ready()
{
    //Optimize for MMD defaults
    setSkeletonPath(getSkeletonPath());
    if (skeleton != nullptr)
    {
        if (hipBone == -1)
        {
            hipBone = skeleton->find_bone("Hips");
            if (hipBone >= 0)
            {
                skeleton->set_bone_disable_rest(hipBone, true);
            }
        }
        if (spineBones.size() == 0)
        {
            int64_t spine = skeleton->find_bone("Spine");
            int64_t chest = skeleton->find_bone("Chest");
            if (spine != -1)
            {
                spineBones.append(spine);
            }
            if (chest != -1)
            {
                spineBones.append(chest);
            }
        }
        if (neckBones.size() == 0)
        {
            int64_t neck = skeleton->find_bone("Neck");
            if (neck != -1)
            {
                neckBones.append(neck);
            }
        }
        if (headBone == -1)
        {
            headBone = skeleton->find_bone("Head");
        }
        if (shoulderRightBone == -1)
        {
            shoulderRightBone = skeleton->find_bone("Right shoulder");
        }
        if (upperArmRightBone == -1)
        {
            upperArmRightBone = skeleton->find_bone("Right arm");
        }
        if (forearmRightBone == -1)
        {
            forearmRightBone = skeleton->find_bone("Right elbow");
        }
        if (handRightBone == -1)
        {
            handRightBone = skeleton->find_bone("Right wrist");
        }
        if (thighRightBone == -1)
        {
            thighRightBone = skeleton->find_bone("Right leg");
        }
        if (shinRightBone == -1)
        {
            shinRightBone = skeleton->find_bone("Right knee");
        }
        if (footRightBone == -1)
        {
            footRightBone = skeleton->find_bone("Right ankle");
        }
        if (shoulderLeftBone == -1)
        {
            shoulderLeftBone = skeleton->find_bone("Left shoulder");
        }
        if (upperArmLeftBone == -1)
        {
            upperArmLeftBone = skeleton->find_bone("Left arm");
        }
        if (forearmLeftBone == -1)
        {
            forearmLeftBone = skeleton->find_bone("Left elbow");
        }
        if (handLeftBone == -1)
        {
            handLeftBone = skeleton->find_bone("Left wrist");
        }
        if (thighLeftBone == -1)
        {
            thighLeftBone = skeleton->find_bone("Left leg");
        }
        if (shinLeftBone == -1)
        {
            shinLeftBone = skeleton->find_bone("Left knee");
        }
        if (footLeftBone == -1)
        {
            footLeftBone = skeleton->find_bone("Left ankle");
        }
    }

    //Setters and getters have validation checks that need to be run;
    //setSkeletonPath(getSkeletonPath());

    setHeadTargetNode(getHeadTargetNode());
    setHandLeftTargetNode(getHandLeftTargetNode());
    setHandRightTargetNode(getHandRightTargetNode());
    setHipTargetNode(getHipTargetNode());
    setFootLeftTargetNode(getFootLeftTargetNode());
    setFootRightTargetNode(getFootRightTargetNode());

    setHeadBone(getHeadBone());
    setNeckBones(getNeckBones());
    setShoulderLeftBone(getShoulderLeftBone());
    setUpperArmLeftBone(getUpperArmLeftBone());
    setForearmLeftBone(getForearmLeftBone());
    setHandLeftBone(getHandLeftBone());
    setShoulderRightBone(getShoulderRightBone());
    setUpperArmRightBone(getUpperArmRightBone());
    setForearmRightBone(getForearmRightBone());
    setHandRightBone(getHandRightBone());
    setSpineBones(getSpineBones());
    setHipBone(getHipBone());
    setThighLeftBone(getThighLeftBone());
    setShinLeftBone(getShinLeftBone());
    setFootLeftBone(getFootLeftBone());
    setThighRightBone(getThighRightBone());
    setShinRightBone(getShinRightBone());
    setFootRightBone(getFootRightBone());
}

void HumanIK::_physics_process(float delta)
{
    time += delta;
    performBodyIK();
    performLeftHandIK();
    performRightHandIK();
    performLeftFootIK();
    performRightFootIK();
}

bool HumanIK::validateBone(int64_t bone)
{
    bool output = false;
    if (skeleton != nullptr)
    {
        if (-1 < bone && bone < skeleton->get_bone_count())
        {
            output = true;
        }
    }
    return output;
}

bool HumanIK::validateBody()
{
    bool output = false;
    if (validateBone(headBone) && validateBone(hipBone))
    {
        int64_t prevBone = headBone;

        Array sortedNeckBones = Array(neckBones);
        sortedNeckBones.sort();
        for (int64_t bone = 0; bone < neckBones.size(); bone++)
        {
            int64_t parentBone = skeleton->get_bone_parent(prevBone);
            if (validateBone(parentBone) && Variant(parentBone) == sortedNeckBones.pop_back())
            {
                prevBone = parentBone;
            }
            else
            {
                return false;
            }
        }

        Array sortedSpineBones = Array(spineBones);
        sortedSpineBones.sort();
        for (int64_t bone = 0; bone < spineBones.size(); bone++)
        {
            int64_t parentBone = skeleton->get_bone_parent(prevBone);
            if (validateBone(parentBone) && Variant(parentBone) == sortedSpineBones.pop_back())
            {
                prevBone = parentBone;
            }
            else
            {
                return false;
            }
        }

        int64_t parentBone = skeleton->get_bone_parent(prevBone);
        if (parentBone == hipBone && spineBones.size() > 0)
        {
            output = true;
        }
    }

    if (validBody == true)
    {
        calcSpineCurve();
    }

    return output;
}

bool HumanIK::validateLeftHand()
{
    bool output = false;
    if (validateBone(handLeftBone) && validateBone(shoulderLeftBone))
    {
        Array sortedSpineBones = Array(spineBones);
        sortedSpineBones.sort();
        Variant topSpine = sortedSpineBones.pop_back();

        int64_t shoulderParent = skeleton->get_bone_parent(shoulderLeftBone);
        int64_t upperArmParent = skeleton->get_bone_parent(upperArmLeftBone);
        int64_t forearmParent = skeleton->get_bone_parent(forearmLeftBone);
        int64_t handParent = skeleton->get_bone_parent(handLeftBone);

        if (handParent == forearmLeftBone && forearmParent == upperArmLeftBone && upperArmParent == shoulderLeftBone && Variant(shoulderParent) == topSpine)
        {
            output = true;
        }
    }
    return output;
}

bool HumanIK::validateRightHand()
{
    bool output = false;
    if (validateBone(handRightBone) && validateBone(shoulderRightBone))
    {
        Array sortedSpineBones = Array(spineBones);
        sortedSpineBones.sort();
        Variant topSpine = sortedSpineBones.pop_back();

        int64_t shoulderParent = skeleton->get_bone_parent(shoulderRightBone);
        int64_t upperArmParent = skeleton->get_bone_parent(upperArmRightBone);
        int64_t forearmParent = skeleton->get_bone_parent(forearmRightBone);
        int64_t handParent = skeleton->get_bone_parent(handRightBone);
        if (handParent == forearmRightBone && forearmParent == upperArmRightBone && upperArmParent == shoulderRightBone && Variant(shoulderParent) == topSpine)
        {
            output = true;
        }
    }
    return output;
}

bool HumanIK::validateLeftFoot()
{
    bool output = false;
    if (validateBone(hipBone) && validateBone(footLeftBone))
    {
        int64_t thighLeftParent = skeleton->get_bone_parent(thighLeftBone);
        int64_t shinLeftParent = skeleton->get_bone_parent(shinLeftBone);
        int64_t footLeftParent = skeleton->get_bone_parent(footLeftBone);

        if (footLeftParent == shinLeftBone && shinLeftParent == thighLeftBone && thighLeftParent == hipBone)
        {
            output = true;
        }
    }
    return output;
}

bool HumanIK::validateRightFoot()
{
    bool output = false;
    if (validateBone(hipBone) && validateBone(footRightBone))
    {
        int64_t thighRightParent = skeleton->get_bone_parent(thighRightBone);
        int64_t shinRightParent = skeleton->get_bone_parent(shinRightBone);
        int64_t footRightParent = skeleton->get_bone_parent(footRightBone);

        if (footRightParent == shinRightBone && shinRightParent == thighRightBone && thighRightParent == hipBone)
        {
            output = true;
        }
    }
    return output;
}

void HumanIK::performBodyIK(float influence)
{
    if (validBody)
    {
        if (headTrackerEnabled)
        {
            if (headTargetSpatial != nullptr)
            {
                headTarget = headTargetSpatial->get_global_transform();
            }

            Transform headPose = skeleton->get_bone_global_pose(headBone);
            Transform hipPose = skeleton->get_bone_global_pose(hipBone);

            std::vector<Transform> chainBones = std::vector<Transform>(spineBones.size() + neckBones.size());
            for (int i = 0; i < spineBones.size(); i++)
            {
                Transform oldTransform = skeleton->get_bone_rest(spineBones[i]);
                chainBones[i] = oldTransform;
            }
            for (int i = 0; i < neckBones.size(); i++)
            {
                Transform oldTransform = skeleton->get_bone_rest(neckBones[i]);
                chainBones[i + spineBones.size()] = oldTransform;
            }
            headPose = headPose.interpolate_with(skeleton->get_global_transform().affine_inverse() * headTarget, influence);
            skeleton->set_bone_global_pose(headBone, headPose); //for some reason I can't set the global pose

            if (hipTrackerEnabled)
            {
                if (hipTargetSpatial != nullptr)
                {
                    hipTarget = hipTargetSpatial->get_global_transform();
                }
                hipPose = hipPose.interpolate_with(skeleton->get_global_transform().affine_inverse() * hipTarget, influence);
                skeleton->set_bone_pose(hipBone, hipPose); //for some reason I can't set the global pose

                Transform headRest = skeleton->get_bone_rest(headBone);
                float twist = 0.5;
                float twistOffset = 0.0;
                std::vector<Transform> solution = solveFABRIK(hipPose, chainBones, headRest, headTarget, 0.0001, 20, twist, twistOffset);
                //DEBUG
                solution = solveISFABRIK(hipPose, chainBones, headRest, headTarget, spineCurve, curveDist, spineLength, 0.0001, 20, twist, twistOffset);
                for (int i = 0; i < spineBones.size(); i++)
                {
                    Transform t = solution[i];
                    skeleton->set_bone_custom_pose(spineBones[i], t);
                    //skeleton->set_bone_pose(spineBones[i], t);
                }
                for (int i = 0; i < neckBones.size(); i++)
                {
                    Transform t = solution[i + spineBones.size()];
                    skeleton->set_bone_custom_pose(neckBones[i], t);
                    //skeleton->set_bone_pose(neckBones[i], t);
                }
            }
            else
            {
                //TODO: make up a hip target based on crouch settings and head position
                //First crouch or lay down based on head height - Solve for head and hips
                //Then check to see what the legs should be doing
                LegTrace();
            }
        }
    }
}

void HumanIK::performLeftHandIK(float influence)
{
    if (validLeftHand)
    {
        if (leftHandTrackerEnabled && handLeftTargetSpatial != nullptr)
        {
            handLeftTarget = handLeftTargetSpatial->get_global_transform();
        }
        Transform handLeftRest = skeleton->get_bone_rest(handLeftBone);
        Transform shoulderGlobal = skeleton->get_bone_global_pose(shoulderLeftBone);
        Transform upperArmLeftPose = skeleton->get_bone_rest(upperArmLeftBone);
        Transform forearmLeftPose = skeleton->get_bone_rest(forearmLeftBone);
        Transform handLeftPose = skeleton->get_bone_global_pose(handLeftBone).interpolate_with(skeleton->get_global_transform().affine_inverse() * handLeftTarget, influence);
        TrigSolution solution = solveTrigIK(shoulderGlobal, upperArmLeftPose, forearmLeftPose, handLeftRest, handLeftPose, elbowAngleOffset, elbowRestOffset, shoulderTwist, wristTwist, elbowPoleOffset, Vector3(elbowPositionBias[0], elbowPositionBias[1], elbowPositionBias[2]), elbowRotationBias);
        skeleton->set_bone_custom_pose(upperArmLeftBone, solution.limb1);
        skeleton->set_bone_custom_pose(forearmLeftBone, solution.limb2);
        skeleton->set_bone_global_pose(handLeftBone, handLeftPose);
    }
}

void HumanIK::performRightHandIK(float influence)
{
    if (validRightHand)
    {
        if (rightHandTrackerEnabled && handRightTargetSpatial != nullptr)
        {
            handRightTarget = handRightTargetSpatial->get_global_transform();
        }
        Transform handRightRest = skeleton->get_bone_rest(handRightBone);
        Transform shoulderGlobal = skeleton->get_bone_global_pose(shoulderRightBone);
        Transform upperArmRightPose = skeleton->get_bone_rest(upperArmRightBone);
        Transform forearmRightPose = skeleton->get_bone_rest(forearmRightBone);
        Transform handRightPose = skeleton->get_bone_global_pose(handRightBone).interpolate_with(skeleton->get_global_transform().affine_inverse() * handRightTarget, influence);
        //a lot of things are multiplied by -1 or have their z multiplied by -1 to reflect everything so that the right hand and the left hand are properly mirrored
        TrigSolution solution = solveTrigIK(shoulderGlobal, upperArmRightPose, forearmRightPose, handRightRest, handRightPose, elbowAngleOffset * -1, elbowRestOffset * -1, shoulderTwist, wristTwist, elbowPoleOffset * -1, Vector3(1 * elbowPositionBias[0], elbowPositionBias[1], elbowPositionBias[2] * -1), elbowRotationBias);
        skeleton->set_bone_custom_pose(upperArmRightBone, solution.limb1);
        skeleton->set_bone_custom_pose(forearmRightBone, solution.limb2);
        skeleton->set_bone_global_pose(handRightBone, handRightPose);
    }
}

void HumanIK::performLeftFootIK(float influence)
{
    if (validLeftFoot)
    {
        if (leftFootTrackerEnabled && footLeftTargetSpatial != nullptr)
        {
            footLeftTarget = footLeftTargetSpatial->get_transform();
        }
        Transform footLeftRest = skeleton->get_bone_rest(footLeftBone);
        Transform hipGlobal = skeleton->get_bone_global_pose(hipBone);
        Transform thighLeftPose = skeleton->get_bone_rest(thighLeftBone);
        Transform shinLeftPose = skeleton->get_bone_rest(shinLeftBone);
        Transform footLeftPose = skeleton->get_bone_global_pose(footLeftBone).interpolate_with(skeleton->get_global_transform().affine_inverse() * footLeftTarget, influence);
        //a lot of things are multiplied by -1 or have their z multiplied by -1 to reflect everything so that the right foot and the left foot are properly mirrored
        TrigSolution solution = solveTrigIK(hipGlobal, thighLeftPose, shinLeftPose, footLeftRest, footLeftPose, kneeAngleOffset, kneeRestOffset, hipTwist, ankleTwist, kneePoleOffset, kneePositionBias, kneeRotationBias);
        skeleton->set_bone_custom_pose(thighLeftBone, solution.limb1);
        skeleton->set_bone_custom_pose(shinLeftBone, solution.limb2);
        skeleton->set_bone_global_pose(footLeftBone, footLeftPose);
    }
}

void HumanIK::performRightFootIK(float influence)
{
    if (validRightFoot)
    {
        if (rightFootTrackerEnabled && footRightTargetSpatial != nullptr)
        {
            footRightTarget = footRightTargetSpatial->get_transform();
        }

        Transform footRightRest = skeleton->get_bone_rest(footRightBone);
        Transform hipGlobal = skeleton->get_bone_global_pose(hipBone);
        Transform thighRightPose = skeleton->get_bone_rest(thighRightBone);
        Transform shinRightPose = skeleton->get_bone_rest(shinRightBone);
        Transform footRightPose = skeleton->get_bone_global_pose(footRightBone).interpolate_with(skeleton->get_global_transform().affine_inverse() * footRightTarget, influence);
        //a lot of things are multiplied by -1 or have their z multiplied by -1 to reflect everything so that the right foot and the left foot are properly mirrored
        TrigSolution solution = solveTrigIK(hipGlobal, thighRightPose, shinRightPose, footRightRest, footRightPose, kneeAngleOffset * -1, kneeRestOffset * -1, hipTwist, ankleTwist, kneePoleOffset * -1, Vector3(1 * kneePositionBias[0], kneePositionBias[1], kneePositionBias[2] * -1), kneeRotationBias);
        skeleton->set_bone_custom_pose(thighRightBone, solution.limb1);
        skeleton->set_bone_custom_pose(shinRightBone, solution.limb2);
        skeleton->set_bone_global_pose(footRightBone, footRightPose);
    }
}

NodePath HumanIK::getSkeletonPath()
{
    return skeletonPath;
}

void HumanIK::setSkeletonPath(NodePath newValue)
{
    skeletonPath = newValue;
    Node *skeletonNode = get_node(skeletonPath);
    skeleton = Object::cast_to<Skeleton>(skeletonNode);

    if (skeleton != nullptr && hipBone >= 0)
    {
        skeleton->set_bone_disable_rest(hipBone, true);
    }
}

NodePath HumanIK::getHeadTargetNode()
{
    return headTargetNode;
}
NodePath HumanIK::getHandLeftTargetNode()
{
    return handLeftTargetNode;
}
NodePath HumanIK::getHandRightTargetNode()
{
    return handRightTargetNode;
}
NodePath HumanIK::getHipTargetNode()
{
    return hipTargetNode;
}
NodePath HumanIK::getFootLeftTargetNode()
{
    return footLeftTargetNode;
}
NodePath HumanIK::getFootRightTargetNode()
{
    return footRightTargetNode;
}

void HumanIK::setHeadTargetNode(NodePath newValue)
{
    headTargetNode = newValue;
    Node *targetNode = get_node_or_null(newValue);
    headTargetSpatial = Object::cast_to<Spatial>(targetNode);
    if (headTargetSpatial != nullptr)
    {
        headTrackerEnabled = true;
    }
}
void HumanIK::setHandLeftTargetNode(NodePath newValue)
{
    handLeftTargetNode = newValue;
    Node *targetNode = get_node_or_null(newValue);
    handLeftTargetSpatial = Object::cast_to<Spatial>(targetNode);
    if (handLeftTargetSpatial != nullptr)
    {
        leftHandTrackerEnabled = true;
    }
}
void HumanIK::setHandRightTargetNode(NodePath newValue)
{
    handRightTargetNode = newValue;
    Node *targetNode = get_node_or_null(newValue);
    handRightTargetSpatial = Object::cast_to<Spatial>(targetNode);
    if (handRightTargetSpatial != nullptr)
    {
        rightHandTrackerEnabled = true;
    }
}
void HumanIK::setHipTargetNode(NodePath newValue)
{
    hipTargetNode = newValue;
    Node *targetNode = get_node_or_null(newValue);
    hipTargetSpatial = Object::cast_to<Spatial>(targetNode);
    if (hipTargetSpatial != nullptr)
    {
        hipTrackerEnabled = true;
    }
}
void HumanIK::setFootLeftTargetNode(NodePath newValue)
{
    footLeftTargetNode = newValue;
    Node *targetNode = get_node_or_null(newValue);
    footLeftTargetSpatial = Object::cast_to<Spatial>(targetNode);
    if (footLeftTargetSpatial != nullptr)
    {
        leftFootTrackerEnabled = true;
    }
}
void HumanIK::setFootRightTargetNode(NodePath newValue)
{
    footRightTargetNode = newValue;
    Node *targetNode = get_node_or_null(newValue);
    footRightTargetSpatial = Object::cast_to<Spatial>(targetNode);
    if (footRightTargetSpatial != nullptr)
    {
        rightFootTrackerEnabled = true;
    }
}

int64_t HumanIK::getHeadBone()
{
    return headBone;
}
PoolIntArray HumanIK::getNeckBones()
{
    return neckBones;
}
int64_t HumanIK::getShoulderLeftBone()
{
    return shoulderLeftBone;
}
int64_t HumanIK::getUpperArmLeftBone()
{
    return upperArmLeftBone;
}
int64_t HumanIK::getForearmLeftBone()
{
    return forearmLeftBone;
}
int64_t HumanIK::getHandLeftBone()
{
    return handLeftBone;
}
int64_t HumanIK::getShoulderRightBone()
{
    return shoulderRightBone;
}
int64_t HumanIK::getUpperArmRightBone()
{
    return upperArmRightBone;
}
int64_t HumanIK::getForearmRightBone()
{
    return forearmRightBone;
}
int64_t HumanIK::getHandRightBone()
{
    return handRightBone;
}
PoolIntArray HumanIK::getSpineBones()
{
    return spineBones;
}
int64_t HumanIK::getHipBone()
{
    return hipBone;
}
int64_t HumanIK::getThighLeftBone()
{
    return thighLeftBone;
}
int64_t HumanIK::getShinLeftBone()
{
    return shinLeftBone;
}
int64_t HumanIK::getFootLeftBone()
{
    return footLeftBone;
}
int64_t HumanIK::getThighRightBone()
{
    return thighRightBone;
}
int64_t HumanIK::getShinRightBone()
{
    return shinRightBone;
}
int64_t HumanIK::getFootRightBone()
{
    return footRightBone;
}

void HumanIK::setHeadBone(int64_t newValue)
{
    headBone = newValue;
    validBody = validateBody();
}
void HumanIK::setNeckBones(PoolIntArray newValue)
{
    neckBones = newValue;
    validBody = validateBody();
}
void HumanIK::setShoulderLeftBone(int64_t newValue)
{
    shoulderLeftBone = newValue;
    validLeftHand = validateLeftHand();
}
void HumanIK::setUpperArmLeftBone(int64_t newValue)
{
    upperArmLeftBone = newValue;
    validLeftHand = validateLeftHand();
}
void HumanIK::setForearmLeftBone(int64_t newValue)
{
    forearmLeftBone = newValue;
    validLeftHand = validateLeftHand();
}
void HumanIK::setHandLeftBone(int64_t newValue)
{
    handLeftBone = newValue;
    validLeftHand = validateLeftHand();
}
void HumanIK::setShoulderRightBone(int64_t newValue)
{
    shoulderRightBone = newValue;
    validRightHand = validateRightHand();
}
void HumanIK::setUpperArmRightBone(int64_t newValue)
{
    upperArmRightBone = newValue;
    validRightHand = validateRightHand();
}
void HumanIK::setForearmRightBone(int64_t newValue)
{
    forearmRightBone = newValue;
    validRightHand = validateRightHand();
}
void HumanIK::setHandRightBone(int64_t newValue)
{
    handRightBone = newValue;
    validRightHand = validateRightHand();
}
void HumanIK::setSpineBones(PoolIntArray newValue)
{
    spineBones = newValue;
    validBody = validateBody();
    validLeftHand = validateLeftHand();
    validRightHand = validateRightHand();
}
void HumanIK::setHipBone(int64_t newValue)
{
    hipBone = newValue;
    validBody = validateBody();
    validLeftFoot = validateLeftFoot();
    validRightFoot = validateRightFoot();
}
void HumanIK::setThighLeftBone(int64_t newValue)
{
    thighLeftBone = newValue;
    validLeftFoot = validateLeftFoot();
}
void HumanIK::setShinLeftBone(int64_t newValue)
{
    shinLeftBone = newValue;
    validLeftFoot = validateLeftFoot();
}
void HumanIK::setFootLeftBone(int64_t newValue)
{
    footLeftBone = newValue;
    validLeftFoot = validateLeftFoot();
}
void HumanIK::setThighRightBone(int64_t newValue)
{
    thighRightBone = newValue;
    validRightFoot = validateRightFoot();
}
void HumanIK::setShinRightBone(int64_t newValue)
{
    shinRightBone = newValue;
    validRightFoot = validateRightFoot();
}
void HumanIK::setFootRightBone(int64_t newValue)
{
    footRightBone = newValue;
    validRightFoot = validateRightFoot();
}

// void HumanIK::_validate_property(PropertyInfo &property) const
// {

//     if (property.name == "root_bone" || property.name == "tip_bone")
//     {

//         if (skeleton)
//         {

//             String names("--,");
//             for (int i = 0; i < skeleton->get_bone_count(); i++)
//             {
//                 if (i > 0)
//                     names += ",";
//                 names += skeleton->get_bone_name(i);
//             }

//             property.hint = PROPERTY_HINT_ENUM;
//             property.hint_string = names;
//         }
//         else
//         {

//             property.hint = PROPERTY_HINT_NONE;
//             property.hint_string = "";
//         }
//     }
// }

HumanIK::TrigSolution HumanIK::solveTrigIK(Transform parent, Transform limb1, Transform limb2, Transform leaf, Transform target, float angleOffset, float restOffset, float l1Twist, float l2Twist, Vector3 poleOffset, Vector3 targetPositionInfluence, float targetRotationInfluence)
{
    //l1 is the final custom transform of the bone nearest the root
    //l2 is the final custom transform of the bone nearest the leaf
    Transform l1 = Transform();
    Transform l2 = Transform();

    Transform localTarget = ((parent * limb1).affine_inverse() * target);
    Vector3 targetVector = localTarget.get_origin(); //targetVector pointing from the root to the leaf

    //Find out which direction the limbs are pointing in their resting position
    Vector3 l1Vector = limb2.get_origin();
    Vector3 l2Vector = leaf.get_origin();

    //Everything we need to use trignometry to find the angles in the triangle made by the bent limb.
    float limb1Length = l1Vector.length();
    float limb2Length = l2Vector.length();
    float totalDistance = targetVector.length();

    /* -- Elbow/Knee Angles and Poles --
    We want to solve for the direction elbow or knee direction deterministically, meaning every possible leaf position in a limb's range of motion has one and only one angle that we'll consider to be the most natural looking.
    If we imagine the limb's total range of motion as an incomplete sphere, we can represent the most natural direction for the elbow or knee to be pointing as unit vectors in the sphere.
    According to the Hair Ball Theorem, it is impossible draw vectors on a sphere such that there are no poles.
    In testing, a pole naturally occurs opposite the resting position of a limb and the limb acts erratically when the limb approaches this area.
    In the following block of code, we offset the the limbs before solving IK such that the poles lie outside the limb's natural range of motion.
    */
    Basis poleRotation = Basis(poleOffset * (Math_PI / 180));
    l1 = l1 * poleRotation;
    Vector3 rotatedL1Vector = poleRotation.xform(l1Vector);

    //Straighten the limb and point it at the target position
    Vector3 cross1;
    float dot1;
    crossDot(cross1, dot1, rotatedL1Vector, targetVector);
    l1.rotate_basis(cross1.normalized(), acos(dot1));

    Vector3 cross2;
    float dot2;
    crossDot(cross2, dot2, l2Vector, l1Vector);
    l2.rotate_basis(cross2.normalized(), acos(dot2));

    //Figure out where the elbow or knee is supposed to bend. The axis1 is the same as axis2, but one is local to l1 and the other is local to l2
    Vector3 axis2 = l2.get_basis()[0].rotated(l2.get_basis()[2], Math_PI * angleOffset / 180);
    Vector3 axis1 = l1.xform(axis2);

    if (totalDistance <= abs(limb1Length - limb2Length))
    {
        //Solving for the angles are impossible here, so just get as close as possible
        l2.rotate_basis(axis2, acos(dot2) + Math_PI);
    }
    else if (totalDistance < limb1Length + limb2Length)
    {
        //Law of Cosines
        float a1 = acos((powf(limb1Length, 2) + powf(totalDistance, 2) - powf(limb2Length, 2)) / (2 * limb1Length * totalDistance));
        float a2 = acos((powf(limb1Length, 2) + powf(limb2Length, 2) - powf(totalDistance, 2)) / (2 * limb1Length * limb2Length)) - Math_PI;
        l1.rotate_basis(axis1, a1);
        l2.rotate_basis(axis2, a2);
    }

    //The resulting elbow/knee angle isn't always the most natural, so we tweak it below based on the target's position and rotation
    Vector3 positionDiff = (parent.affine_inverse() * target).get_origin() - ((limb1 * limb2) * leaf).get_origin(); //local to parent

    // Dude rotations are hard. WTF
    // Vector3 localTargetVector = (l1 * limb2 * l2).get_basis().xform_inv(targetVector).normalized();     //local to l2
    // Basis localTargeBasis = ((parent * limb1 * l1 * limb2 * l2).affine_inverse() * target).get_basis(); //local to l2
    // Basis leafProjections = Basis(normalProjection(leaf.get_basis()[0], localTargetVector), normalProjection(leaf.get_basis()[1], localTargetVector), normalProjection(leaf.get_basis()[2], localTargetVector));
    // Basis targetProjections = Basis(normalProjection(localTargeBasis[0], localTargetVector), normalProjection(localTargeBasis[1], localTargetVector), normalProjection(localTargeBasis[2], localTargetVector));
    // Basis projectionDiff = targetProjections - leafProjections;
    // Basis projectionCross = Basis(leafProjections[0].cross(targetProjections[0]), leafProjections[1].cross(targetProjections[1]), leafProjections[2].cross(targetProjections[2]));
    // Vector3 rotationDiff = Vector3();
    // if ((projectionCross[0].dot(localTargetVector)) > 0)
    // {
    //     printf("dot: %f\n", projectionCross[0].dot(localTargetVector));
    //     float maxValue = targetProjections[0].length() * leafProjections[0].length();
    //     float angle = (targetProjections[0].dot(leafProjections[0]) - maxValue) / (2 * maxValue);
    //     if (angle < -1)
    //     {
    //         angle = -1;
    //     }
    //     else if (angle > 1)
    //     {
    //         angle = 1;
    //     }
    //     rotationDiff[0] = asin(angle) / (2 * Math_PI);
    // }
    // else
    // {
    //     printf("dot -1: %f\n", projectionCross[0].dot(localTargetVector));
    //     float maxValue = targetProjections[0].length() * leafProjections[0].length();
    //     float angle = (maxValue - targetProjections[0].dot(leafProjections[0])) / (2 * maxValue);
    //     if (angle < -1)
    //     {
    //         angle = -1;
    //     }
    //     else if (angle > 1)
    //     {
    //         angle = 1;
    //     }
    //     rotationDiff[0] = asin(angle) / (2 * Math_PI);
    // }
    // if ((projectionCross[1].dot(localTargetVector)) > 0)
    // {
    //     rotationDiff[1] = projectionCross[1].length();
    // }
    // else
    // {
    //     rotationDiff[1] = projectionCross[1].length() * -1;
    // }
    // if ((projectionCross[2].dot(localTargetVector)) > 0)
    // {
    //     rotationDiff[2] = projectionCross[2].length();
    // }
    // else
    // {
    //     rotationDiff[2] = projectionCross[2].length() * -1;
    // }
    // printf("projection diff: (%f %f %f) (%f %f %f) (%f %f %f)\n", projectionDiff[0][0], projectionDiff[0][1], projectionDiff[0][2], projectionDiff[1][0], projectionDiff[1][1], projectionDiff[1][2], projectionDiff[2][0], projectionDiff[2][1], projectionDiff[2][2]);
    // printf("rotation diff: (%f %f %f)\n", rotationDiff[0], rotationDiff[1], rotationDiff[2]);

    float totalPositionOffset = (smoothCurve(positionDiff[0]) * targetPositionInfluence[0] * Math_PI / 180) + (smoothCurve(positionDiff[1]) * targetPositionInfluence[1] * Math_PI / 180) + (smoothCurve(positionDiff[2]) * targetPositionInfluence[2] * Math_PI / 180);
    //float totalRotationOffset = (smoothCurve(rotationDiff[0]) * targetRotationInfluence / 10); //   +(smoothCurve(rotationDiff[1]) * targetRotationInfluence * Math_PI / 180) + (smoothCurve(rotationDiff[2]) * targetRotationInfluence * Math_PI / 180);
    l1.rotate_basis(targetVector.normalized(), (restOffset * Math_PI / 180) + totalPositionOffset);

    //Detwist - Account for lack of twist joints and put some of the twisting on the elbow/knee joint instead of keeping it all the wrists and shoulder
    Vector3 newL1Vector = l1.xform(l1Vector).normalized();
    float l1TwistAmount = l1.get_basis().orthonormalized().get_euler()[2] * ((100 - l1Twist) / 100);
    l1.rotate_basis(newL1Vector, l1TwistAmount);
    l2.rotate_basis(l1.xform_inv(newL1Vector), l1TwistAmount * -1);

    Vector3 newL2Vector = l2.xform(l2Vector).normalized();
    float l2TwistAmount = localTarget.get_basis().orthonormalized().get_euler()[2] * ((100 - l2Twist) / 100);
    l2.rotate_basis(newL2Vector, l2TwistAmount * -1);

    return TrigSolution{l1, l2};
}

float HumanIK::smoothCurve(float number)
{
    return number / (abs(number) + 0.5);
}

Vector3 HumanIK::normalProjection(Vector3 v, Vector3 normal)
{
    Vector3 proj = normal.normalized() * (normal.dot(v) / normal.length_squared());
    return v - proj;
}

std::vector<Transform> HumanIK::solveISFABRIK(Transform parent, std::vector<Transform> restTransforms, Transform leaf, Transform target, std::vector<Transform> curveTransforms, float curveDist, float maxDist, float threshold, int loopLimit, float twist, float twistOffset) //Start Interpolated FABRIK. Interpolate chain towards idealized curve, then solve FABRIK to fill in the rest. Helps to prevent locking and make sure joints bend in the right direction without constraints.
{
    std::vector<Transform> transformArray = std::vector<Transform>(restTransforms.size(), Transform());
    if (restTransforms.size() > 0 && curveTransforms.size() == restTransforms.size() && curveDist != maxDist)
    {
        float x = parent.xform(Transform(restTransforms[0]).get_origin()).distance_to(target.get_origin());
        float y = (maxDist - x) / (maxDist - curveDist);
        if (y > 0)
        {
            y = sqrtf(y);
        }

        //FABRIK only works on points, so we turn all our bone transforms into points.
        std::vector<BonePoint> bonePoints = std::vector<BonePoint>(restTransforms.size());
        float maxLength = 0;

        Transform globalT = parent;
        Transform interpolatedT = parent;
        Vector3 rootPoint = Vector3();
        Vector3 leafPoint = Vector3();

        for (int i = 0; i < restTransforms.size(); i++)
        {
            Transform t = Transform(restTransforms[i]);
            Transform c = Transform(curveTransforms[i]);
            Transform interp = t.interpolate_with(c, y);

            globalT = globalT * t;
            interpolatedT = interpolatedT * interp;
            Vector3 globalOrigin = globalT.get_origin();
            globalOrigin = interpolatedT.get_origin();

            float length = 0;
            if (i == restTransforms.size() - 1)
            {
                //On the last item, the leaf is the child
                length = leaf.get_origin().length();
                leafPoint = globalT.xform(leaf.get_origin());
            }
            else
            {
                length = Transform(restTransforms[i + 1]).get_origin().length();
            }
            maxLength += length;

            bonePoints[i] = BonePoint(length, t, globalOrigin);

            if (i == 0)
            {
                //The parent doesn't rotate, so the root of the point chain is actually the root of the first child
                rootPoint = globalOrigin;
            }
        }

        float targetLength = (target.get_origin() - rootPoint).length();
        if (targetLength >= maxDist)
        {
            //Just point the whole chain at the target
            Vector3 restDirection = (parent * restTransforms[0]).xform_inv(leafPoint);
            restDirection = Vector3(0, 0, -1);
            Vector3 targetDirection = (parent * restTransforms[0]).xform_inv(target.get_origin());
            printf("target: %f %f %f\n", targetDirection[0], targetDirection[1], targetDirection[2]);
            Transform localTransform = Transform();
            Vector3 cross = Vector3();
            float dot = 0;
            crossDot(cross, dot, restDirection, targetDirection);
            localTransform.rotate_basis(cross.normalized(), acos(dot));
            transformArray[0] = localTransform.orthonormalized();
        }
        else
        {
            solveFABRIKPoints(bonePoints, rootPoint, target.get_origin(), threshold, loopLimit); //does FABRIK on it and bonePoints through pass-by-reference

            //Now we have to take all the points in bonePoints and turn them back into Transforms.
            //Because each bone will be pointing directly at the bone in front of it on the chain this is pretty simple.
            //However, there's still one degree of freedom that needs to be taken care of and that is the rotation of the bone along the axis in which it's pointing a.k.a. twist.
            //We calculate twist by using Quaternions to figure out the smoothest transition from the root twist amount to the leaf twist amount
            Transform localT = parent;
            Transform globalLeaf = globalT * leaf;
            Quat totalRotation = (globalLeaf.affine_inverse() * target).get_basis();           //Total rotational difference between the target and the leaf at rest expressed in Quaternions
            float totalTwistLength = parent.get_origin().distance_to(globalLeaf.get_origin()); //total length of the limb including the parent. If we didn't include the parent then the first joint wouldn't twist and we want twisting to be as spread out as possible.
            float localTwistLength = 0;                                                        //how far up the limb we are. used to figure out how much we should be twisting

            //variables reused by every bone in the iteration
            Vector3 direction = Vector3();
            Vector3 restDirection = Vector3();
            Vector3 cross = Vector3();
            float dot = 1;

            for (int i = 0; i < bonePoints.size(); i++)
            {
                BonePoint b = bonePoints[i];
                printf("bonePoint %i: %f %f %f - %f\n", i, bonePoints[i].point[0], bonePoints[i].point[1], bonePoints[i].point[2], bonePoints[i].transform.get_origin().length());
                //First we rotate everything to match the target
                localTwistLength += b.length;
                Transform t = Quat().slerp(totalRotation, (localTwistLength * twist + twistOffset) / totalTwistLength); //The output transform
                //Then we rotate everything back so that -Z is still -Z. The twist should stay.
                crossDot(cross, dot, t.get_basis()[2], Vector3(0, 0, 1));
                t.rotate_basis(cross, acos(dot) * -1); //don't know why the -1 fixed everything but it works now lol

                localT = localT * b.transform;
                if (i == bonePoints.size() - 1)
                {
                    //the last
                    direction = localT.xform_inv(target.get_origin());
                    restDirection = leaf.get_origin();
                }
                else
                {
                    direction = localT.xform_inv(bonePoints[i + 1].point);
                    restDirection = bonePoints[i + 1].transform.get_origin();
                }
                crossDot(cross, dot, restDirection, direction);
                t.rotate_basis(cross, acos(dot));
                transformArray[i] = t.orthonormalized();
                localT = localT * t;
            }
        }
    }
    return transformArray;
}

void HumanIK::calcSpineCurve()
{
    if (validBody)
    {
        //get a copy of all the bones
        Transform head = skeleton->get_bone_rest(headBone);

        Transform totalTransform = Transform();
        std::vector<BonePoint> bonePoints = std::vector<BonePoint>(spineBones.size() + neckBones.size());
        float totalLength = head.get_origin().length();
        for (int i = 0; i < spineBones.size(); i++)
        {
            Transform t = skeleton->get_bone_rest(spineBones[i]);
            if (i > 0)
            { // exclude the hip bone transform
                totalTransform = totalTransform * t;
                totalLength += t.get_origin().length();
            }
            else
            {
                totalTransform = t.get_basis(); //ignore translation
            }

            if (i < spineBones.size() - 1)
            {
                float l = skeleton->get_bone_rest(spineBones[i + 1]).get_origin().length();
                bonePoints[i] = BonePoint(l, t, totalTransform.get_origin());
            }
            else if (neckBones.size() > 0)
            {
                float l = skeleton->get_bone_rest(neckBones[0]).get_origin().length();
                bonePoints[i] = BonePoint(l, t, totalTransform.get_origin());
            }
            else
            {
                float l = head.get_origin().length();
                bonePoints[i] = BonePoint(l, t, totalTransform.get_origin());
            }
        }
        for (int i = 0; i < neckBones.size(); i++)
        {
            Transform t = skeleton->get_bone_rest(neckBones[i]);

            totalTransform = totalTransform * t;
            totalLength += t.get_origin().length();

            if (i < neckBones.size() - 1)
            {
                float l = skeleton->get_bone_rest(neckBones[i + 1]).get_origin().length();
                bonePoints[i + spineBones.size()] = BonePoint(l, t, totalTransform.get_origin());
            }
            else
            {
                float l = head.get_origin().length();
                bonePoints[i + spineBones.size()] = BonePoint(l, t, totalTransform.get_origin());
            }
        }

        spineLength = totalTransform.xform(head.get_origin()).length(); //at rest how far is the base of the head from the root of the chain (the hip bone)
        double radius = totalLength / Math_PI;
        float totalAngle = 0;
        float totalBAngle = 0;
        float lastB = 0;
        Vector3 nCurveAxis = Vector3(curveAxis.normalized()[0], curveAxis.normalized()[1], 0);
        spineCurve.assign(bonePoints.size(), Transform());

        for (int i = bonePoints.size() - 1; i >= 0; i--)
        {
            float a = 2 * asin(bonePoints[i].length / (2 * radius));
            float b = (Math_PI - a) / 2;
            totalAngle += a;

            if (i < bonePoints.size() - 1)
            {
                Transform bone = bonePoints[i + 1].transform;
                Vector3 idealAngle = Vector3(0, 0, -1).rotated(nCurveAxis, lastB + b); //TODO: change -1 to 1
                Vector3 cross = Vector3();
                float dot = 0;
                crossDot(cross, dot, bone.get_basis()[2], idealAngle);
                bone.rotate_basis(cross, acos(dot));
                spineCurve[i + 1] = bone.orthonormalized();
            }

            if (i == 0)
            {
                Transform bone = bonePoints[i].transform;
                float xa = 2 * Math_PI - totalAngle;
                curveDist = 2 * radius * sin(2 * xa);
                float xb = (Math_PI - xa) / 2;
                Vector3 idealAngle = Vector3(0, 0, 1).rotated(nCurveAxis, (xb + b));
                Vector3 cross = Vector3();
                float dot = 0;
                crossDot(cross, dot, bone.get_basis()[2], idealAngle);
                bone.rotate_basis(cross, acos(dot));
                spineCurve[i] = bone.orthonormalized();
            }

            lastB = b;
        }
    }
}

Vector3 HumanIK::fitPointToLine(Vector3 point, Vector3 goal, float length)
{
    Vector3 output = Vector3();
    Vector3 direction = point - goal;
    output = direction.normalized() * length;
    output = goal + output;
    return output;
}

void HumanIK::solveFABRIKPoints(std::vector<HumanIK::BonePoint> &bonePoints, Vector3 rootPoint, Vector3 goal, float threshold, int loopLimit)
{
    int loop = 0;
    while (abs((goal - bonePoints.back().point).length() - bonePoints.back().length) > threshold && loop < loopLimit)
    {
        loop++;
        // Backward
        Vector3 target = goal;                          //The very first bone in the backwards iteration needs to reach the goal
        for (int i = bonePoints.size() - 1; i > 0; i--) //don't place the root bone point
        {
            BonePoint b = bonePoints[i];
            b.point = fitPointToLine(b.point, target, b.length);
            bonePoints[i] = b;
            target = b.point; //Where this new point ended up will be the target for the next bone in the chain
        }

        // Forward
        target = rootPoint;                         //The root bone must touch the root point
        for (int i = 1; i < bonePoints.size(); i++) //i starts at 1 because the root bone is fixed in place
        {
            BonePoint b = bonePoints[i];
            b.point = fitPointToLine(b.point, target, b.transform.get_origin().length());
            bonePoints[i] = b;
            target = b.point;
        }
    }
}

std::vector<Transform> HumanIK::solveFABRIK(Transform parent, std::vector<Transform> restTransforms, Transform leaf, Transform target, float threshold, int loopLimit, float twist, float twistOffset)
{
    std::vector<Transform> transformArray = restTransforms;
    std::vector<BonePoint> bonePoints = std::vector<BonePoint>(restTransforms.size());

    //FABRIK only works on points, so we turn all our bone transforms into points.
    if (restTransforms.size() > 0)
    {
        float maxLength = 0;

        Transform globalT = parent;
        Vector3 rootPoint = Vector3();

        for (int i = 0; i < restTransforms.size(); i++)
        {
            Transform t = restTransforms[i];
            globalT = globalT * t;
            Vector3 globalOrigin = globalT.get_origin();

            float length = 0;
            if (i == restTransforms.size() - 1)
            {
                //On the last item, the leaf is the child
                length = leaf.get_origin().length();
            }
            else
            {
                length = Transform(restTransforms[i + 1]).get_origin().length();
            }
            maxLength += length;

            bonePoints[i] = BonePoint(length, t, globalOrigin);

            if (i == 0)
            {
                //The parent doesn't rotate, so the root of the point chain is actually the root of the first child
                rootPoint = globalOrigin;
            }
        }

        float targetLength = (target.get_origin() - rootPoint).length();
        if (targetLength >= maxLength)
        {
            //Just make it straight
            Transform localTransform = Transform();
            for (int i = 0; i < transformArray.size(); i++)
            {
                Transform t = Transform();
                Vector3 targetDirection = Vector3(0, 0, -1);
                Vector3 direction = Vector3(0, 0, -1);
                if (i == 0)
                {
                    targetDirection = Transform(restTransforms[i]).xform_inv(parent.xform_inv(target.get_origin()));
                }
                else
                {
                    // targetDirection = Transform(restTransforms[i]).get_origin();
                }
                if (i == transformArray.size() - 1)
                {
                    direction = leaf.get_origin();
                }
                else
                {
                    direction = Transform(restTransforms[i + 1]).get_origin();
                }
                targetDirection.normalize();
                direction.normalize();
                Vector3 cross;
                float dot;
                crossDot(cross, dot, direction, targetDirection);
                t.rotate_basis(cross.normalized(), acos(dot));
                transformArray[i] = t;
            }
        }
        else
        {
            solveFABRIKPoints(bonePoints, rootPoint, target.get_origin(), threshold, loopLimit); //does FABRIK on it and bonePoints through pass-by-reference

            //Now we have to take all the points in bonePoints and turn them back into Transforms.
            //Because each bone will be pointing directly at the bone in front of it on the chain this is pretty simple.
            //However, there's still one degree of freedom that needs to be taken care of and that is the rotation of the bone along the axis in which it's pointing a.k.a. twist.
            //We calculate twist by using Quaternions to figure out the smoothest transition from the root twist amount to the leaf twist amount
            Transform localT = parent;
            Transform globalLeaf = globalT * leaf;
            Quat totalRotation = (globalLeaf.affine_inverse() * target).get_basis();           //Total rotational difference between the target and the leaf at rest expressed in Quaternions
            float totalTwistLength = parent.get_origin().distance_to(globalLeaf.get_origin()); //total length of the limb including the parent. If we didn't include the parent then the first joint wouldn't twist and we want twisting to be as spread out as possible.
            float localTwistLength = 0;                                                        //how far up the limb we are. used to figure out how much we should be twisting

            //variables reused by every bone in the iteration
            Vector3 direction = Vector3();
            Vector3 restDirection = Vector3();
            Vector3 cross = Vector3();
            float dot = 1;

            for (int i = 0; i < bonePoints.size(); i++)
            {
                BonePoint b = bonePoints[i];

                //First we rotate everything to match the target
                localTwistLength += b.length;
                Transform t = Transform(Quat().slerp(totalRotation, (localTwistLength * twist + twistOffset) / totalTwistLength)); //The output transform
                //Then we rotate everything back so that -Z is still -Z. The twist should stay.
                crossDot(cross, dot, t.get_basis()[2], Vector3(0, 0, 1));
                t.rotate(cross.normalized(), acos(dot) * -1); //don't know why the -1 fixed everything but god damn it works now

                localT = localT * Transform(b.transform);
                if (i == bonePoints.size() - 1)
                {
                    //the last
                    direction = localT.xform_inv(target.get_origin());
                    restDirection = leaf.get_origin();
                }
                else
                {
                    direction = localT.xform_inv(bonePoints[i + 1].point);
                    restDirection = bonePoints[i + 1].transform.get_origin();
                }
                crossDot(cross, dot, restDirection, direction);
                t.rotate_basis(cross.normalized(), acos(dot));
                transformArray[i] = t.orthonormalized();
                localT = localT * t;
            }
        }
    }
    return transformArray;
}

void HumanIK::crossDot(Vector3 &cross, float &dot, Vector3 srcVector, Vector3 targetVector)
{
    srcVector.normalize();
    targetVector.normalize();
    cross = srcVector.cross(targetVector);
    dot = srcVector.dot(targetVector);
    if (cross.length() == 0)
    {
        if (dot < 0)
        { //the vectors are 180 degrees away from each other
            if (targetVector[0] == 0)
            {
                cross = srcVector.cross(Vector3(1, 0, 0));
            }
            else
            {
                cross = srcVector.cross(Vector3(0, 1, 0));
            }
        }
        else
        {
            cross = Vector3(0, 0, 1);
            dot = 1;
        }
    }
    cross.normalize();
}

void HumanIK::LegTrace()
{
}

bool HumanIK::checkBalance()
{
    return true;
}

bool HumanIK::checkFall()
{
    return false;
}